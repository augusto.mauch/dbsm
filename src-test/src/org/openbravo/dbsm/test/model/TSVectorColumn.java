package org.openbravo.dbsm.test.model;

/*
 ************************************************************************************
 * Copyright (C) 2020 Openbravo S.L.U.
 * Licensed under the Apache Software License version 2.0
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to  in writing,  software  distributed
 * under the License is distributed  on  an  "AS IS"  BASIS,  WITHOUT  WARRANTIES  OR
 * CONDITIONS OF ANY KIND, either  express  or  implied.  See  the  License  for  the
 * specific language governing permissions and limitations under the License.
 ************************************************************************************
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.openbravo.dbsm.test.base.DbsmTest;

public class TSVectorColumn extends DbsmTest {

  public TSVectorColumn(String rdbms, String driver, String url, String sid, String user,
      String password, String name) throws FileNotFoundException, IOException {
    super(rdbms, driver, url, sid, user, password, name);
  }

  /**
   * Test sourcedata export of a table with a TSVector column to a file in /tmp folder
   */
  @Test
  public void testSourceDataFTS() throws SQLException, IOException {
    resetDB();
    String model = "fullTextSearch/BASE_MODEL.xml";
    List<String> adTables = Arrays.asList("TEST");

    updateDatabase(model, "data/datachanges/fullTextSearch", adTables);
    exportDatabase("/tmp/exportDB");
    assertExportIsConsistent(model);
  }
}
